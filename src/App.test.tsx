import { render, screen } from "@testing-library/react";
import App from "./App";

describe("App", () => {
  test("renders learn react link", () => {
    render(<App />);
    const linkElement = screen.getByText(/Your/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("redners not found page for invalid url", () => {
    window.location.pathname = "/kajdshsalkjd/asdshjaklsdjash";
    render(<App />);
    const linkElement = screen.getByText(/Page Not Found/i);
    expect(linkElement).toBeInTheDocument();
  });
});
