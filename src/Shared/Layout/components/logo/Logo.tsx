import { FC } from "react";
import "./Logo.css";

const Logo: FC = () => (
  <div>
    <div className="logo">Kleiner</div>
    <div className="sub">
      <div className="sub_text">Your URL Shortener</div>
    </div>
  </div>
);

export default Logo;
