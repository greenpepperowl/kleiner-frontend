import { render, screen } from "@testing-library/react";
import Logo from "./Logo";

test("Correctly renders the logo", () => {
  render(<Logo />);
  const element = screen.getByText(/Kleiner/i);
  expect(element).toBeInTheDocument();
});
