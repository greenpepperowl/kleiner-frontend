import { FC } from "react";
import Logo from "./components/logo/Logo";
import "./Layout.css";

interface LayoutProps {
  childComp?: React.ReactNode;
}

const Layout: FC<LayoutProps> = ({ childComp }) => (
  <div>
    <header className="site-header">
      <Logo />
    </header>
    <div>{childComp}</div>
  </div>
);

export default Layout;
