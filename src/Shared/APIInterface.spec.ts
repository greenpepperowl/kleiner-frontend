jest.unmock("axios");
import MockAdapter from "axios-mock-adapter";
import http from "../http-common";
import API from "./APIInterface";

describe("API.getSlug", () => {
  let mock: MockAdapter;

  beforeAll(() => {
    mock = new MockAdapter(http);
  });

  afterEach(() => {
    mock.reset();
  });

  describe("Success case", () => {
    it("should return the SmallURL object", async () => {
      const slug = "slug";
      const smallURLResponse = {
        data: {
          base_url: "http://google.com",
          slug: slug,
        },
      };
      mock.onGet(`/api/small_urls/${slug}`).reply(200, smallURLResponse);

      const result = await API.getSlug(slug);

      expect(mock.history.get[0].url).toEqual(`/api/small_urls/${slug}`);
      expect(result).toEqual(smallURLResponse.data);
    });
  });

  describe("Failure case", () => {
    it("should throw an error", async () => {
      const slug = "slug";
      const smallURLResponse = {
        data: {
          error: "not found",
        },
      };
      mock.onGet(`/api/small_urls/${slug}`).reply(404, smallURLResponse);

      const result = await API.getSlug(slug);

      expect(mock.history.get[0].url).toEqual(`/api/small_urls/${slug}`);
      expect(result).toEqual(smallURLResponse);
    });
  });
});

describe("API.createSlug", () => {
  let mock: MockAdapter;

  beforeAll(() => {
    mock = new MockAdapter(http);
  });

  afterEach(() => {
    mock.reset();
  });

  describe("Success case", () => {
    it("should return the SmallURL object", async () => {
      const slug = "slug";
      const base_url = "http://google.com";
      const smallURLResponse = {
        data: {
          base_url,
          slug,
        },
      };
      mock.onPost("/api/small_urls").reply(201, smallURLResponse);

      const result = await API.createSlug(slug, base_url);

      expect(mock.history.post[0].url).toEqual(`/api/small_urls`);
      expect(mock.history.post[0].data).toEqual(
        JSON.stringify({ small_url: { base_url, slug } })
      );
      expect(result).toEqual(smallURLResponse.data);
    });
  });

  describe("Failure case", () => {
    it("should return an error object", async () => {
      const slug = "slug";
      const base_url = "invalid_url";
      const smallURLResponse = {
        errors: {
          base_url: ["invalid url"],
          slug: [],
        },
      };
      mock.onPost("/api/small_urls").reply(422, smallURLResponse);

      const result = await API.createSlug(slug, base_url);

      expect(mock.history.post[0].url).toEqual(`/api/small_urls`);
      expect(mock.history.post[0].data).toEqual(
        JSON.stringify({ small_url: { base_url, slug } })
      );
      expect(result).toEqual(smallURLResponse);
    });
  });
});
