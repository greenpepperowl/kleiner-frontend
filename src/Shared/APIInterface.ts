import http from "../http-common";

interface SmallURL {
  data: {
    base_url: string;
    slug: string;
  };
}

class API {
  getSlug(slug: string) {
    return http
      .get<SmallURL>(`/api/small_urls/${slug}`)
      .then((response) => response.data.data)
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 404) {
            return error.response.data;
          }
        }
        return error;
      });
  }

  createSlug(slug: string, base_url: string) {
    return http
      .post<SmallURL>("/api/small_urls", {
        small_url: {
          base_url,
          slug,
        },
      })
      .then((response) => response.data.data)
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 404 || error.response.status === 422) {
            return error.response.data;
          }
        }
        return error;
      });
  }
}

export default new API();
