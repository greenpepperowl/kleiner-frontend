import { render, screen, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import URLForm, { checkSlugUnique } from "./URLForm";
import APIInterface from "../../../../Shared/APIInterface";

describe("checkSlugUnique", () => {
  it("returns true if the slug is undefined", () => {
    return checkSlugUnique("").then((response) =>
      expect(response).toEqual(true)
    );
  });

  it("returns true if the slug wasn't found on the backend", () => {
    jest.spyOn(APIInterface, "getSlug").mockReturnValue(
      Promise.resolve({
        errors: {
          detail: "Not Found",
        },
      })
    );

    return checkSlugUnique("test_slug").then((response) =>
      expect(response).toEqual(true)
    );
  });

  it("returns false if backend throws an error", () => {
    jest.spyOn(APIInterface, "getSlug").mockImplementation(() => {
      throw new Error();
    });

    return checkSlugUnique("test_slug").then((response) =>
      expect(response).toEqual(false)
    );
  });
});

describe("URLForm", () => {
  it("renders and submits successfully with valid data", async () => {
    const handleSubmit = jest.fn();
    const { getByTestId } = render(<URLForm onSubmit={handleSubmit} />);

    jest.spyOn(APIInterface, "getSlug").mockReturnValue(
      Promise.resolve({
        errors: {
          detail: "Not Found",
        },
      })
    );

    userEvent.type(screen.getByLabelText(/url/i), "http://google.com");
    userEvent.type(screen.getByLabelText(/alias/i), "test_alias");

    userEvent.click(getByTestId("button-create"));

    await waitFor(() =>
      expect(handleSubmit).toHaveBeenCalledWith(
        {
          url: "http://google.com",
          alias: "test_alias",
        },
        expect.anything()
      )
    );
  });

  it("renders an error when the url is not valid", async () => {
    const handleSubmit = jest.fn();
    render(<URLForm onSubmit={handleSubmit} />);

    userEvent.type(screen.getByLabelText(/url/i), "google.com");

    await waitFor(() => {
      expect(screen.queryByText("url must be a valid URL")).not.toBeNull();
    });
  });

  it("No error if alias is available", async () => {
    const slug = "test_slug";
    const handleSubmit = jest.fn();

    render(<URLForm onSubmit={handleSubmit} />);

    jest.spyOn(APIInterface, "getSlug").mockReturnValue(
      Promise.resolve({
        errors: {
          detail: "Not Found",
        },
      })
    );

    userEvent.type(screen.getByLabelText(/alias/i), slug);

    await waitFor(() => {
      expect(screen.queryByText("Alias already exists")).toBeNull();
    });
  });

  it("Renders an error if alias is unavailable", async () => {
    const slug = "test_slug";
    const handleSubmit = jest.fn();

    render(<URLForm onSubmit={handleSubmit} />);

    const smallURLResponse = {
      data: {
        base_url: "http://google.com",
        slug,
      },
    };

    jest
      .spyOn(APIInterface, "getSlug")
      .mockReturnValue(Promise.resolve(smallURLResponse));

    userEvent.type(screen.getByLabelText(/alias/i), slug);

    await waitFor(() => {
      expect(screen.queryByText("Alias already exists")).not.toBeNull();
    });
  });
});
