import { Button, TextField } from "@material-ui/core";
import { useFormik } from "formik";
import { FC, useRef } from "react";
import * as yup from "yup";
import "./URLForm.css";
import APIInterface from "../../../../Shared/APIInterface";
import { FormValues, URLFormInterface } from "../../types/form";

// https://github.com/formium/foformik/issues/512
// Work around to not run api check constantly when a user
// enters a character
const cacheTest = (asyncValidate: any) => {
  let _valid = false;
  let _value = "";

  return async (value: any) => {
    if (value !== _value) {
      const response = await asyncValidate(value);
      _value = value;
      _valid = response;
      return response;
    }
    return _valid;
  };
};

export const checkSlugUnique = async (slug: string) => {
  if (slug !== "" && slug !== undefined) {
    try {
      const response = await APIInterface.getSlug(slug);
      const slugAvailable =
        response.errors && response.errors.detail === "Not Found";
      return slugAvailable;
    } catch (e) {
      return false;
    }
  } else {
    return true;
  }
};

const URLForm: FC<URLFormInterface> = ({ onSubmit }) => {
  const initialValues: FormValues = { url: "", alias: "" };
  const slugTest = useRef(cacheTest(checkSlugUnique));
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: yup.object({
      url: yup.string().url().defined("URL is required"),
      alias: yup
        .string()
        .test("checkDupSlug", "Alias already exists", slugTest.current),
    }),
    onSubmit: (values, actions) => {
      onSubmit(values, actions);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit} className="form">
      <TextField
        fullWidth
        id="url"
        name="url"
        label="URL"
        error={formik.touched.url && Boolean(formik.errors.url)}
        variant="outlined"
        helperText={Boolean(formik.errors.url) && formik.errors.url}
        value={formik.values.url}
        onChange={formik.handleChange}
      />
      <TextField
        fullWidth
        id="alias"
        name="alias"
        label="Alias (Optional)"
        error={formik.touched.alias && Boolean(formik.errors.alias)}
        variant="outlined"
        helperText={Boolean(formik.errors.alias) && formik.errors.alias}
        value={formik.values.alias}
        onChange={formik.handleChange}
      />
      <Button
        color="primary"
        variant="contained"
        fullWidth
        type="submit"
        data-testid="button-create"
      >
        Create
      </Button>
    </form>
  );
};

export default URLForm;
