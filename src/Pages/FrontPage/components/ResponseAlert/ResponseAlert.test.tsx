import { render, screen } from "@testing-library/react";
import ResponseAlert from "./ResponseAlert";

test("renders correct title with success", () => {
  render(
    <ResponseAlert
      severity={"success"}
      message={"message"}
      closeHandler={jest.fn()}
    />
  );
  const linkElement = screen.getByText(/Success/i);
  expect(linkElement).toBeInTheDocument();
});

test("renders correct title with error", () => {
  render(
    <ResponseAlert
      severity={"error"}
      message={"message"}
      closeHandler={jest.fn()}
    />
  );
  const linkElement = screen.getByText(/Error/i);
  expect(linkElement).toBeInTheDocument();
});
