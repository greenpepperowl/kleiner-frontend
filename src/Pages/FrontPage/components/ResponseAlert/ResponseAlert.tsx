import { Alert, AlertTitle } from "@material-ui/lab";
import { FC } from "react";
import { ResponseInterface } from "../../types/reponse";

const ResponseAlert: FC<
  ResponseInterface & { closeHandler: (arg0: boolean) => void }
> = ({ severity, message, closeHandler }) => {
  const title = severity === "success" ? "Success" : "Error";
  return (
    <Alert
      severity={severity}
      onClose={() => closeHandler(false)}
      data-testid="response-alert"
    >
      <AlertTitle>{title}</AlertTitle>
      {message}
    </Alert>
  );
};

export default ResponseAlert;
