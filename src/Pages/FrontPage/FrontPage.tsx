import { FC } from "react";
import "./FrontPage.css";
import FormContainer from "./containers/FormContainer";

const FrontPage: FC = () => (
  <div className="box flex">
    <FormContainer />
  </div>
);

export default FrontPage;
