import handleReponse from "./ResponseHandler";

describe("responseHandler", () => {
  describe("error cases", () => {
    it("should properly return error severity", () => {
      const response = handleReponse({
        errors: { slug: ["Slug already exists"] },
      });
      expect(response.severity).toEqual("error");
    });

    it("should properly return error severity for base_url", () => {
      const response = handleReponse({ errors: { base_url: ["invalid url"] } });
      expect(response.severity).toEqual("error");
      expect(response.message).toEqual("Invalid URL (The host may not exist)");
    });
  });

  describe("success cases", () => {
    it("should properly handle the success case", () => {
      const response = handleReponse({ slug: "user-slug" });
      expect(response.severity).toEqual("success");
    });
  });
});
