import { ResponseInterface } from "../types/reponse";

interface APIResponse {
  errors?: {
    slug?: [string];
    base_url?: [string];
  };
  slug?: string;
}

// Handle the response from the backend and convert to
// a message that can be shown to the end user
export default function handleReponse(
  response: APIResponse
): ResponseInterface {
  const baseURL = process.env["REACT_APP_URL"];
  if (response.errors) {
    if (
      response.errors.slug &&
      response.errors.slug[0] === "Slug already exists"
    ) {
      return {
        severity: "error",
        message: "Alias already exists",
      };
    } else if (
      response.errors.base_url &&
      response.errors.base_url[0] === "invalid url"
    ) {
      return {
        severity: "error",
        message: "Invalid URL (The host may not exist)",
      };
    } else {
      return {
        severity: "error",
        message: "Unexpected error",
      };
    }
  } else {
    const slug = response.slug;
    return {
      severity: "success",
      message: `KleinURL: ${baseURL}/${slug}`,
    };
  }
}
