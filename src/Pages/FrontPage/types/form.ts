import { FormikHelpers } from "formik";
export interface FormValues {
  url: string;
  alias: string;
}

export interface URLFormInterface {
  onSubmit: (
    arg0: FormValues,
    actions: FormikHelpers<FormValues>
  ) => Promise<void>;
}
