export interface ResponseInterface {
  severity: "success" | "error";
  message: string;
}
