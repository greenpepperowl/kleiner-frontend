import { render, screen, waitFor } from "@testing-library/react";
import FormContainer from "./FormContainer";

test("URLForm enabled on loadup", async () => {
  render(<FormContainer />);
  const urlForm = screen.getByText(/Create/);
  expect(urlForm).toBeInTheDocument();
});

test("ResponseAlert disabled on loadup", async () => {
  const { queryByTestId } = render(<FormContainer />);
  await waitFor(() => {
    expect(queryByTestId("response-alert")).toBeNull();
  });
});
