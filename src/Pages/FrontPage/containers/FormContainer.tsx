import { FC, useState } from "react";
import APIInterface from "../../../Shared/APIInterface";
import ResponseAlert from "../components/ResponseAlert/ResponseAlert";
import handleReponse from "../apiHandlers/ResponseHandler";
import URLForm from "../components/URLForm/URLForm";
import { ResponseInterface } from "../types/reponse";

const FormContainer: FC = () => {
  const [submitted, setSubmitted] = useState(false);
  const [state, setState] = useState<ResponseInterface>({
    severity: "success",
    message: "",
  });

  return (
    <div>
      <URLForm
        onSubmit={async (values, actions) => {
          const { url, alias } = values;
          const response = await APIInterface.createSlug(alias, url);
          const newState = handleReponse(response);
          setState(newState);
          actions.resetForm();
          setSubmitted(true);
        }}
      />
      {submitted && (
        <ResponseAlert
          severity={state.severity}
          message={state.message}
          closeHandler={setSubmitted}
        />
      )}
    </div>
  );
};

export default FormContainer;
