import { render, screen, waitFor } from "@testing-library/react";
import APIInterface from "../../Shared/APIInterface";
import RedirectPage from "./RedirectPage";

describe("RedirectPage", () => {
  it("shows loading spinner on initial load", async () => {
    const test_slug = "slug";
    const { getByTestId } = render(<RedirectPage slug={test_slug} />);

    await waitFor(() => {
      const spinner = getByTestId("loading-spinner");
      expect(spinner).toBeInTheDocument();
    });
  });

  it("Redirects to page if a match is found on the API", async () => {
    const slug = "google";
    const base_url = "http://google.com/";

    jest.spyOn(console, "error").mockImplementation(() => {});

    jest.spyOn(APIInterface, "getSlug").mockReturnValue(
      Promise.resolve({
        base_url,
        slug,
      })
    );

    render(<RedirectPage slug={slug} />);

    await waitFor(() => {
      expect(window.location.href).toEqual(base_url);
    });
  });

  it("Renders the NotFoundPage if it's not a valid alias", async () => {
    jest.spyOn(APIInterface, "getSlug").mockReturnValue(
      Promise.resolve({
        errors: {
          detail: "Not Found",
        },
      })
    );

    render(<RedirectPage slug={"test_slug"} />);

    await waitFor(() => {
      const NotFoundPage = screen.getByText(/not found/i);
      expect(NotFoundPage).toBeInTheDocument();
    });
  });
});
