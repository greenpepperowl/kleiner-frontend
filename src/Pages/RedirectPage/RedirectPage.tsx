import { FC, useEffect, useRef, useState } from "react";
import APIInterface from "../../Shared/APIInterface";
import Layout from "../../Shared/Layout/Layout";
import NotFoundPage from "../NotFoundPage/NotFoundPage";
import LoadingSpinner from "./components/LoadingSpinner";
import { RedirectInterface } from "./types/redirect";

const RedirectPage: FC<RedirectInterface> = ({ slug }) => {
  const [loading, setLoading] = useState(true);
  let isRendered = useRef<boolean>(false);

  useEffect(() => {
    isRendered.current = true;
    APIInterface.getSlug(slug).then((response) => {
      if (response.base_url) {
        window.location.href = response.base_url;
      } else if (isRendered.current) {
        setLoading(false);
      }
    });
    return () => {
      isRendered.current = false;
    };
  }, [slug]);

  if (loading) {
    return <LoadingSpinner />;
  } else {
    return (
      <Layout
        childComp={<NotFoundPage message={`Alias ${slug} doesn't exist`} />}
      />
    );
  }
};

export default RedirectPage;
