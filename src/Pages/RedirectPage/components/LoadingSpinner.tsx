import { FC } from "react";
import { CircularProgress } from "@material-ui/core";

const LoadingSpinner: FC = () => (
  <CircularProgress data-testid="loading-spinner" />
);

export default LoadingSpinner;
