import { render, screen } from "@testing-library/react";
import NotFoundPage from "./NotFoundPage";

test("Correctly renders message", () => {
  render(<NotFoundPage message={"Mock message"} />);
  const message = screen.getByText(/Mock message/i);
  expect(message).toBeInTheDocument();
});
