import { FC } from "react";

interface NotFoundPageInterface {
  message?: string;
}

const NotFoundPage: FC<NotFoundPageInterface> = ({ message }) => (
  <>
    <div>
      <h3>Page Not Found</h3>
    </div>
    <div>{message}</div>
  </>
);

export default NotFoundPage;
