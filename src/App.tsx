import { FC } from "react";
import "./App.css";
import { Route, Switch } from "wouter";
import Layout from "./Shared/Layout/Layout";
import FrontPage from "./Pages/FrontPage/FrontPage";
import RedirectPage from "./Pages/RedirectPage/RedirectPage";
import NotFoundPage from "./Pages/NotFoundPage/NotFoundPage";

const App: FC = () => (
  <div className="App">
    <Switch>
      <Route path="/">
        <Layout childComp={<FrontPage />} />
      </Route>
      <Route path="/:slug">
        {(params) => <RedirectPage slug={params.slug} />}
      </Route>
      <Route path="/:rest*">
        {(params) => (
          <Layout
            childComp={
              <NotFoundPage message={`Page ${params.rest} doesn't exist`} />
            }
          />
        )}
      </Route>
    </Switch>
  </div>
);

export default App;
