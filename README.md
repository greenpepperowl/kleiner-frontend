# Kleiner Frontend

This is the front-end for Kleiner, to test this locally you'll also need to have the [Kleiner backend](https://gitlab.com/greenpepperowl/kleiner) running.

**See a live example [here](https://amazing-golick-70bff4.netlify.app/)**

## Setup
To setup the project we recommend using the Makefile. To use the Makefile you'll need to have Docker installed on your system. Once you have the site running, you can access it at `http://localhost:8000`.

### Makefile instructions

| Instruction | Command |
| --| -- |
| Run the server | `$ make server` |
| Run the tests | `$ make test` |

### Local setup
To run the project locally we recommended using [asdf](https://github.com/asdf-vm/asdf), [asdf-nodejs](https://github.com/asdf-vm/asdf-nodejs) and [asdf-yarn](https://github.com/twuni/asdf-yarn) as these
will help to ensure that you have the correct version of node and yarn running.

Once these are installed, run `asdf install` to install the build dependencies.

| Instruction | Command |
| - | - |
| Install build dependencies | `$ asdf install` |
| Install build packages | `$ yarn install` |
| Start the server | `$ yarn start` |
| Run tests | `$ yarn test` |
| Check Coverage | `$ yarn test -- --coverage --watchAll` |

### Docker setup
The Makefile calls out to Docker, if you'd like to use Docker directly you will need the following commands
| Instruction | Command |
| - | - |
| Start project | `$ docker-compose up kleiner_frontend`
| Run tests | `$ docker-compose up test`
| Clean the build | `$ docker-compose rm`

## Contribution Notes
If using the Makefile or Docker instructions, if you add new dependencies to the project you may need to totally remove the images and 
then rebuild to ensure that the dependencies are included.



## Additional Notes
- [Future Improvements](./docs/future-improvements.md)
- [Additional Notes](./docs/additional-notes.md)