# Additional Notes

## Developer experience
- When pusihng to the repo, husky is run to ensure that the code is formatted as specified in the `Prettier` config.

## CI/CD
- A CI/CD is setup using gitlab-ci to ensure that the build passes before it is deployed.
- The build is created using `yarn build` and then deployed to [`netlify`](https://www.netlify.com/).
