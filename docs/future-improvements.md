# Future Improvements

## General Improvements
- Improve testing coverage of the application
- Do more controlled UI testing
- Improve the user interface
- Create a custom Favicon for the website
- Better user experience if the backend is down, at the moment not totally clear if the backend is down.
- Use a documentation generator similar to how we do on the backend
- Use something like Sentry on the frontend to be able to track any decgredations / bugs
- Move towards using a CSS library to help tidy up the code

## Deployment
- Possibly move away from Netlify, and host on GCP using k8s. 

## Testing
- Configure end-to-end integration tests on a staging environment, using a tool like Cypress.io

## Security 
- Put the system behind Cloudflare to protect against some basic attacks